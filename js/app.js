/* BÀI TẬP 1 */
document.getElementById(`btn-bai-tap-1`).addEventListener(`click`, function() {
    var oddNumbers = [];
    var evenNumbers = [];
    var innerContent = ``;

    /* WHILE loop: */
    var index = 0;
    // while (index >= 0 && index <= 100) {
    //     if (index % 2 === 0) {
    //         evenNumbers.push(index);
    //     } else {
    //         oddNumbers.push(index);
    //     }

    //     index++;
    // }

    /* FOR loop: */
    for (var index = 0; index <= 100; index++) {
        if (index % 2 === 0) {
            evenNumbers.push(index);
        } else {
            oddNumbers.push(index);
        }
    }

    document.getElementById(`ket-qua-bai-tap-1`).innerHTML = innerContent += `<b>Odd numbers:</b> ${oddNumbers}.<br/><b>Even numbers:</b> ${evenNumbers}.`;
});

/* BÀI TẬP 2 */
document.getElementById(`btn-bai-tap-2`).addEventListener(`click`, function() {
    var max = document.getElementById('input-number').value;
    var result = [];
    var innerContent = ``;

    /* WHILE loop: */
    // var index = 1;
    // while (index <= max) {
    //     result.push(index);
    //     index++;
    // }

    /* FOR loop: */
    for (var index = 1; index <= max; index++) {
        result.push(index);
    }

    document.getElementById(`ket-qua-bai-tap-2`).innerHTML = innerContent += `<b>Result:</b> ${result}.`;
});

/* BÀI TẬP 3 */
document.getElementById(`btn-bai-tap-3`).addEventListener(`click`, function() {
    var result = [];
    var innerContext = ``;

    /* WHILE loop: */
    // var index = 0;
    // while (index <= 1000) {
    //     if (index % 3 === 0) {
    //         result.push(index);
    //     }

    //     index++;
    // }

    /* FOR loop: */
    for (var index = 0; index < 1000; index++) {
        if (index % 3 === 0) {
            result.push(index);
        }
    }

    document.getElementById(`ket-qua-bai-tap-3`).innerHTML = innerContext += `<b>Result:</b> ${result}.`;
});

/* BÀI TẬP 4 */
document.getElementById(`btn-bai-tap-4`).addEventListener(`click`, function() {
    var total = 0;
    var count = 0;

    while (total < 10000) {
        count++;
        total += count;
    }

    document.getElementById(`ket-qua-bai-tap-4`).innerHTML = `<b>Result:</b> ${count}.`;
});

/* BÀI TẬP 5 */
function baiTap5() {
    document.getElementById(`btn-bai-tap-5`).addEventListener(`click`, function() {
        var x = document.getElementById(`x`).value;
        var n = document.getElementById(`n`).value;
        var total = 0;

        for (var index = 1; index < n; index++) {
            total += x**n;
        }

        document.getElementById(`ket-qua-bai-tap-5`).innerHTML = `Total: ${total}.`;
    });
}

baiTap5();

/* BÀI TẬP 6 */
function factorial(number) {
    if (number > 0) {
        return number * factorial(number - 1);
    } else {
        return 1;
    }
}

document.getElementById(`btn-bai-tap-6`).addEventListener(`click`, function() {
    var result = factorial(document.getElementById(`bai-tap-6-input-number`).value);
    document.getElementById(`ket-qua-bai-tap-6`).innerHTML = `Factorial: ${result}.`;
});

/* BÀI TẬP 7 */
function changeBackgroundColor() {
    var divs = document.getElementsByClassName(`generated-div`)
    for (var index = 0; index < divs.length; index++){
        if ((index + 1) % 2 == 0){
            divs[index].style.background = `red`;
        } else {
            divs[index].style.background = `blue`;
        }
    }
}

function generateDivsAndAddBackgroundColor(tagName) {
    var resultBlockInnerHtml = document.getElementById(tagName);
    for (var index = 0; index < 10; index++) {
        resultBlockInnerHtml.innerHTML += '<div class="generated-div">Div</div>';
    }

    changeBackgroundColor();
}

document.getElementById(`btn-bai-tap-7`).addEventListener(`click`, function() {
    generateDivsAndAddBackgroundColor(`ket-qua-bai-tap-7`);
});

/* BÀI TẬP 8 */
function isPrime(number) {
    if (number <= 1)
        return false;

    for (var index = 2; index < number; index++) {
        if (number % index == 0)
            return false
    }

    return true;
}

document.getElementById(`btn-bai-tap-8`).addEventListener(`click`, function() {
    var primeNumbers = [];

    var number = document.getElementById(`bai-tap-8-input-number`).value;
    for (var index = 1; index <= number; index++) {
        if (isPrime(index)) {
            primeNumbers.push(index);
        }
    }

    document.getElementById(`ket-qua-bai-tap-8`).innerHTML = primeNumbers;
});